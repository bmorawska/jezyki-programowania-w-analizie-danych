#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# Wczytywanie danych
headings = ['season',
            'age',
            'diseases',
            'accident',
            'surgery',
            'fever',
            'alcohol',
            'smoking',
            'sittingHours',
            'diagnosis']

data = pd.read_csv('../dane/fertility_Diagnosis.data', names=headings)


# Wyznaczanie dominanty dla cech jakościowych
qualityData = data[['season', 'diseases', 'accident', 'surgery', 'fever', 'alcohol', 'smoking', 'diagnosis']]
mode = qualityData.mode()
print("Dominanta cech jakościowych:\n {}\n".format(mode))


# Wyznaczanie mediany, minimum i maksimum dla cech ilościowych
quantityData = data[['age', 'sittingHours']]

median = quantityData.median()
print("Mediana cech ilościowych:\n {}\n".format(median))

minimum = quantityData.min()
print("Minimum cech ilościowych:\n {}\n".format(minimum))

maximum = quantityData.max()
print("Maximum cech ilościowych:\n {}\n".format(minimum))


# Wyznaczanie współczynnika korelacji Pearsona
correlation = quantityData["age"].corr(quantityData["sittingHours"])
print("Współczynnik korelacji Pearsona:\n {}\n".format(correlation))


# Tworzenie histogramów

# Zaokrąglanie wieku do pełnych lat
age = (quantityData.loc[:, 'age'] + 1) * 18
age = age.round()

# Obliczanie częstotliwości wystąpień każdego wieku
bins = age.value_counts()

# Rysowanie histogramu wieku
plt.figure(figsize=(10,7))
plt.bar(bins.index, bins.values, alpha=0.8, color="m", edgecolor="k")
plt.xticks(bins.index) 
plt.grid(axis='y', alpha=0.75)
plt.xlabel("wiek [lata]", FontSize='16')
plt.ylabel("częstotliwość wystąpień", FontSize='16')
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title("Histogram wieku osób poddanych badaniu", FontSize='16')
plt.show()

# Zaokrąglanie czasu spędzanego przed komputerem do pełnych godzin
hours = quantityData.loc[:, 'sittingHours'] * 16
hours = np.round(hours)

# Obliczanie częstotliwości wystąpień każdego czasu zaokrąglona w dół
bins = hours.value_counts()

# Rysowanie histogramu wieku
plt.figure(figsize=(10,7))
plt.bar(bins.index, bins.values, alpha=0.8, color="m", edgecolor="k")
plt.xticks(bins.index) 
plt.grid(axis='y', alpha=0.75)
plt.xlabel("czas [pełne godziny]", FontSize='16')
plt.ylabel("częstotliwość wystąpień", FontSize='16')
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.title("Histogram czasu spędzonego w pozycji siedzącej\n wśród osób poddanych badaniu", FontSize='16')
plt.show()