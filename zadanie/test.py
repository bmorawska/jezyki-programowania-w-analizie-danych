#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import numpy as np
from scipy import stats
from scipy import optimize
import researchpy as rp
import matplotlib.pyplot as plt


# # Zadanie na 5

# In[2]:


# Wczytywanie danych
headings = ['index',
            'sex',
            'bodyweight',
            'heartweight']

data = pd.read_csv('../dane/cats.csv', names=headings, skiprows=1)
data = data[['sex', 'bodyweight', 'heartweight']]
data['bodyweight'] = data['bodyweight'].astype(float)
data['heartweight'] = data['heartweight'].astype(float)
data


# In[3]:


data.groupby("sex")['heartweight'].describe()


# In[4]:


female = data[(data['sex'] == 'F')]
female.reset_index(inplace = True)

male = data[(data['sex'] == 'M')]
male.reset_index(inplace = True)

statistic, pvalue = stats.ttest_ind(female['heartweight'], male['heartweight'])

## https://pythonfordatascience.org/independent-t-test-python/

descriptives, results = rp.ttest(female['heartweight'], male['heartweight'])
results


# In[5]:


# Obliczenie liczby przedziałów dla wagi ciała
bodybins = int((data['bodyweight'].max() - data['bodyweight'].min()) * 10.0) 
bodybins


# In[6]:


# Implementacja funkcji krzywej Gaussa
def gaussian(x, amplitude, mean, stddev):
    return amplitude * np.exp(-((x - mean) / 4 / stddev)**2)


# In[114]:


# Rysowanie histogramu wagi ciała kotów
plt.figure(figsize=(10,7))
plt.xticks(np.arange(data['bodyweight'].min(), data['bodyweight'].max(), 0.1)) 
plt.xticks(fontsize=14, rotation=90)
plt.yticks(fontsize=14)
plt.xlabel("Waga ciała [kilogramy]", FontSize='16')
plt.ylabel("częstotliwość wystąpień", FontSize='16')
N, bins, patches = plt.hist(data['bodyweight'].values, bins=bodybins, alpha=0.8, color="m", edgecolor="k")

# Aproksymacja funkcją gaussowską
fitted_curve_parameters, _ = optimize.curve_fit(gaussian, bins[:len(bins)-1], N)
plt.plot(bins, gaussian(bins, *fitted_curve_parameters), '--', color="b")

plt.grid(axis='y', alpha=0.75)
plt.title("Histogram wagi ciała kotów", FontSize='18')


# In[115]:


# Obliczenie liczby przedziałów dla wagi serca
heartbins = int((data['heartweight'].max() - data['heartweight'].min()) * 2.0) 
heartbins


# In[116]:


# Rysowanie histogramu wagi serca kotów
plt.figure(figsize=(10,7))
plt.xticks(np.arange(data['heartweight'].min(), data['heartweight'].max(), 0.5))

print(len(np.arange(data['heartweight'].min(), data['heartweight'].max(), 0.5)))
plt.xticks(fontsize=14, rotation=90)
plt.yticks(fontsize=14)
plt.xlabel("Waga serca [gramy]", FontSize='16')
plt.ylabel("częstotliwość wystąpień", FontSize='16')
N, bins, patches = plt.hist(data['heartweight'].values, bins=heartbins, alpha=0.8, color="m", edgecolor="k")

# Aproksymacja funkcją gaussowską
fitted_curve_parameters, _ = optimize.curve_fit(gaussian, bins[:len(bins)-1], N)
plt.plot(bins, gaussian(bins, *fitted_curve_parameters), '--', color="b")

plt.grid(axis='y', alpha=0.75)
plt.title("Histogram wagi serca kotów", FontSize='18')


# In[117]:


# Rysowanie histogramu wagi serca kotów (dziewczynki)
plt.figure(figsize=(10,7))
plt.xticks(np.arange(female['heartweight'].min(), female['heartweight'].max(), 0.5)) 
plt.xticks(fontsize=14, rotation=90)
plt.yticks(fontsize=14)
plt.xlabel("Waga serca [gramy]", FontSize='16')
plt.ylabel("częstotliwość wystąpień", FontSize='16')
N, bins, patches = plt.hist(female['heartweight'].values, bins=heartbins, alpha=0.8, color="m", edgecolor="k")

fitted_curve_parameters, _ = optimize.curve_fit(gaussian, bins[:len(bins)-1], N)
plt.plot(bins, gaussian(bins, *fitted_curve_parameters), '--', color="b")

plt.grid(axis='y', alpha=0.75)
plt.title("Histogram wagi serca kotów (samice)", FontSize='18')


# In[118]:


# Rysowanie histogramu wagi serca kotów (chłopcy)
plt.figure(figsize=(10,7))
plt.xticks(np.arange(male['heartweight'].min(), male['heartweight'].max(), 0.5)) 
plt.xticks(fontsize=14, rotation=90)
plt.yticks(fontsize=14)
plt.xlabel("Waga serca [gramy]", FontSize='16')
plt.ylabel("częstotliwość wystąpień", FontSize='16')
N, bins, patches = plt.hist(male['heartweight'].values, bins=heartbins, alpha=0.8, color="m", edgecolor="k")

fitted_curve_parameters, _ = optimize.curve_fit(gaussian, bins[:len(bins)-1], N)
plt.plot(bins, gaussian(bins, *fitted_curve_parameters), '--', color="b")

plt.grid(axis='y', alpha=0.75)
plt.title("Histogram wagi serca kotów (samce)", FontSize='18')


# In[ ]:





# In[ ]:




