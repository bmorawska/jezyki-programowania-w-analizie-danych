#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
from scipy import optimize
from scipy import stats
import matplotlib.pyplot as plt


def gaussian(x, amplitude, mean, stddev):
    return amplitude * np.exp(-((x - mean) / 4 / stddev) ** 2)


# Wczytywanie danych
headings = ['index',
            'sex',
            'bodyweight',
            'heartweight']

data = pd.read_csv('../dane/cats.csv', names=headings, skiprows=1)
data = data[['sex', 'bodyweight', 'heartweight']]
data['bodyweight'] = data['bodyweight'].astype(float)
data['heartweight'] = data['heartweight'].astype(float)

# Podział wczytanych danych na samce i samice
female = data[(data['sex'] == 'F')]
female.reset_index(inplace=True)

male = data[(data['sex'] == 'M')]
male.reset_index(inplace=True)

# Sprawdzenie normalności rozkładów oraz wariancji danych (waga serca) dla obu płci
pValueTest = 0.05 ## 5% poziom istotności statystycznej
_, pValueMale = stats.shapiro(male['heartweight'])
_, pValueFemale = stats.shapiro(female['heartweight'])
if (pValueMale > pValueTest):
    print("Cecha \"waga ciała\" dla samców ma zbliżony rozkład do rozkładu normalnego")
else:
    print("Cecha \"waga ciała\" dla samców nie ma zbliżonego rozkładu do rozkładu normalnego")
if (pValueFemale > pValueTest):
    print("Cecha \"waga ciała\" dla samic ma zbliżony rozkład do rozkładu normalnego")
else:
    print("Cecha \"waga ciała\" dla samic nie ma zbliżonego rozkładu do rozkładu normalnego")

print("Levene: ", stats.levene(male['heartweight'], female['heartweight']))

varianceMale = np.var(male['heartweight'])
varianceFemale = np.var(female['heartweight'])
print("Wariancja danych dla samców: {}".format(round(varianceMale, 5)))
print("Wariancja danych dla samic: {}".format(round(varianceFemale, 5)))

# Przeprowadzenie testu t Welcha na danych
statisticValue, pValue = stats.ttest_ind(female['heartweight'], male['heartweight'], equal_var = False)
print("Badanie cechy \"waga ciała\" dla kotów płci żeńskiej i męskiej:\n")
print("Wartość testu t Welcha: {}".format(statisticValue, 7))
print("Wartość pValue: {}".format(pValue))
if (pValue > pValueTest):
    print("Przy pomocy badanej cechy \"waga ciała\" można rozróżnić płeć kotów")
else:
    print("Przy pomocy badanej cechy \"waga ciała\" nie można rozróżnić płci kotów")

# Obliczenie liczby przedziałów dla wagi ciała
bodybins = int((data['bodyweight'].max() - data['bodyweight'].min()) * 10.0)

# Rysowanie histogramu wagi ciała kotów
plt.figure(figsize=(10, 7))
plt.xticks(np.arange(data['bodyweight'].min(), data['bodyweight'].max(), 0.1))
plt.xticks(fontsize=14, rotation=90)
plt.yticks(fontsize=14)
plt.xlabel("Waga ciała [kilogramy]", FontSize='16')
plt.ylabel("częstotliwość wystąpień", FontSize='16')
N, bins, _ = plt.hist(data['bodyweight'].values, bins=bodybins, alpha=0.8, color="m", edgecolor="k")

# Aproksymacja funkcją gaussowską
fitted_curve_parameters, _ = optimize.curve_fit(gaussian, bins[:len(bins) - 1], N)
plt.plot(bins, gaussian(bins, *fitted_curve_parameters), '--', color="b")

plt.grid(axis='y', alpha=0.75)
plt.title("Histogram wagi ciała kotów", FontSize='18')
plt.show()

# Obliczenie liczby przedziałów dla wagi serca
heartbins = int((data['heartweight'].max() - data['heartweight'].min()) * 2.0)

# Rysowanie histogramu wagi serca kotów
plt.figure(figsize=(10, 7))
plt.xticks(np.arange(data['heartweight'].min(), data['heartweight'].max(), 0.5))
plt.xticks(fontsize=14, rotation=90)
plt.yticks(fontsize=14)
plt.xlabel("Waga serca [gramy]", FontSize='16')
plt.ylabel("częstotliwość wystąpień", FontSize='16')
N, bins, _ = plt.hist(data['heartweight'].values, bins=heartbins, alpha=0.8, color="m", edgecolor="k")

# Aproksymacja funkcją gaussowską
fitted_curve_parameters, _ = optimize.curve_fit(gaussian, bins[:len(bins) - 1], N)
plt.plot(bins, gaussian(bins, *fitted_curve_parameters), '--', color="b")

plt.grid(axis='y', alpha=0.75)
plt.title("Histogram wagi serca kotów", FontSize='18')
plt.show()

# Rysowanie histogramu wagi serca kotów (dziewczynki)
plt.figure(figsize=(10, 7))
plt.xticks(np.arange(female['heartweight'].min(), female['heartweight'].max(), 0.5))
plt.xticks(fontsize=14, rotation=90)
plt.yticks(fontsize=14)
plt.xlabel("Waga serca [gramy]", FontSize='16')
plt.ylabel("częstotliwość wystąpień", FontSize='16')
N, bins, _ = plt.hist(female['heartweight'].values, bins=heartbins, alpha=0.8, color="m", edgecolor="k")

# Aproksymacja funkcją gaussowską
fitted_curve_parameters, _ = optimize.curve_fit(gaussian, bins[:len(bins) - 1], N)
plt.plot(bins, gaussian(bins, *fitted_curve_parameters), '--', color="b")

plt.grid(axis='y', alpha=0.75)
plt.title("Histogram wagi serca kotów (samice)", FontSize='18')
plt.show()

# Rysowanie histogramu wagi serca kotów (chłopcy)
plt.figure(figsize=(10, 7))
plt.xticks(np.arange(male['heartweight'].min(), male['heartweight'].max(), 0.5))
plt.xticks(fontsize=14, rotation=90)
plt.yticks(fontsize=14)
plt.xlabel("Waga serca [gramy]", FontSize='16')
plt.ylabel("częstotliwość wystąpień", FontSize='16')
N, bins, patches = plt.hist(male['heartweight'].values, bins=heartbins, alpha=0.8, color="m", edgecolor="k")

# Aproksymacja funkcją gaussowską
fitted_curve_parameters, _ = optimize.curve_fit(gaussian, bins[:len(bins) - 1], N)
plt.plot(bins, gaussian(bins, *fitted_curve_parameters), '--', color="b")

plt.grid(axis='y', alpha=0.75)
plt.title("Histogram wagi serca kotów (samce)", FontSize='18')
plt.show()
