# Języki programowania w analizie danych
_Zadanie 1_

**Autorzy:**
- Barbara Morawska 234096
- Andrzej Sasinowski 234118

Wydział Fizyki Technicznej, Informatyki i Matematyki Stosowanej

Politechnika Łódzka 

2019/2020 


###Wymagania 
- Python > 3.6
- Pandas (1.0.2)
- Numpy (1.18.2) 
- Scipy (1.4.1) 
- Matplotlib (3.2.0)

###Uruchomienie programu
Aby uruchomic program należy w wejść do katalogu, w którym znajdują
się pliki ``zadanie_na_3.py``, ``zadanie_na_4.py``, ``zadanie_na_5.py``, a następnie wpisać komendę:

_Na systemie Windows_
```python
python [nazwa_programu].py
```

_Na systemach Linux_
```python
python3 [nazwa_programu].py
```